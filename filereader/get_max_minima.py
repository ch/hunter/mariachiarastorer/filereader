from filereader.cube_reader import CubeReader
from filereader.cml_reader import CmlReader
import pandas as pd
import numpy as np
import os
from scipy.spatial import cKDTree


def get_minimum_info(cube):
    index_min = cube.values.argmin()
    xyz_min = cube.xyz[index_min]
    value_min = cube.values[index_min]
    return value_min, xyz_min


def get_minimum_info_next(cube):
    index_min = cube.values.argmin()
    cube.values[index_min] = 1000
    index_next = cube.values.argmin()
    xyz_min = cube.xyz[index_next]
    value_min = cube.values[index_next]
    return value_min, xyz_min

def get_minimum_distance_element(cube, cml):
    value_min, xyz_min = get_minimum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_min)
    return value_min, distance, cml.list_atoms[index].element

def get_minimum_distance_element_not_hydrogen(cube, cml):
    value_min, xyz_min = get_minimum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_min)
    element = cml.list_atoms[index].element
    while element == "H":
        value_min, xyz_min = get_minimum_info_next(cube)
        distance, index = tree.query(xyz_min)
        element = cml.list_atoms[index].element
    return value_min, distance, cml.list_atoms[index].element

def get_minimum_distance_element_select(cube, cml, element_select):
    value_min, xyz_min = get_minimum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_min)
    element = cml.list_atoms[index].element
    count = 0
    while count <len(cube.values):
        if element != element_select:
            value_min, xyz_min = get_minimum_info_next(cube)
            distance, index = tree.query(xyz_min)
            element = cml.list_atoms[index].element
            count += 1
        else:
            count = len(cube.values)
    return value_min, distance, cml.list_atoms[index].element

def get_minimum_distance_atom(cube, cml):
    value_min, xyz_min = get_minimum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_min)
    return value_min, distance, cml.list_atoms[index]

def get_maximum_info(cube):
    index_max = cube.values.argmax()
    xyz_max = cube.xyz[index_max]
    value_max = cube.values[index_max]
    return value_max, xyz_max

def get_maximum_distance_element(cube, cml):
    value_max, xyz_max = get_maximum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_max)
    return value_max, distance, cml.list_atoms[index].element

def get_maximum_distance_atom(cube, cml):
    value_max, xyz_max = get_maximum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_max)
    return value_max, distance, cml.list_atoms[index]

def get_maximum_distance_element_select(cube, cml, element_select):
    value_max, xyz_max = get_maximum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_max)
    element = cml.list_atoms[index].element
    count = 0
    while count <len(cube.values):
        if element != element_select:
            value_max, xyz_max = get_maximum_info_next(cube)
            distance, index = tree.query(xyz_max)
            element = cml.list_atoms[index].element
            count += 1
        else:
            count = len(cube.values)
    return value_max, distance, cml.list_atoms[index].element

'''def get_maximum_distance_atom_select(cube, cml, element_select):
    value_max, xyz_max = get_maximum_info(cube)
    tree = cKDTree(cml.xyz)
    distance, index = tree.query(xyz_max)
    element = cml.list_atoms[index].element
    count = 0
    while count <len(cube.values):
        if element != element_select:
            value_max, xyz_max = get_maximum_info_next(cube)
            distance, index = tree.query(xyz_max)
            element = cml.list_atoms[index].element
            count += 1
        else:
            count = len(cube.values)
    return value_max, distance, cml.list_atoms[index]'''

def get_maximum_from_atom_name(cube, cml, atom_name):
    atom = cml.dict_atoms[atom_name]
    tree = cKDTree(cube.xyz)
    k = round(len(cube.xyz)/len(cml.xyz))
    distance, index = tree.query([atom.xyz], k=k)
    return cube.values[index[0]].max()


def get_minimum_from_atom_name(cube, cml, atom_name):
    atom = cml.dict_atoms[atom_name]
    tree = cKDTree(cube.xyz)
    k = round(len(cube.xyz)/len(cml.xyz))
    distance, index = tree.query([atom.xyz], k=k)
    return cube.values[index[0]].min()


"""def get_minimum_distance_from_atom_name(cube, cml, atom_name):
    atom = cml.dict_atoms[atom_name]
    tree = cKDTree(cube.xyz)
    k = round(len(cube.xyz)/len(cml.xyz))
    distance, index = tree.query([atom.xyz], k=k)
    return cube.values[index[0]].min(), distance[0][0]"""


def get_minimum_distance_from_atom_name(cube, cml, atom_name, distance=2):
    atom = cml.dict_atoms[atom_name]
    tree = cKDTree(cube.xyz)
    k = round(len(cube.xyz)/len(cml.xyz))
    index = tree.query_ball_point([atom.xyz], distance)
    i = cube.values[index[0]].argmin()
    return cube.values[index[0]].min(), np.linalg.norm(atom.xyz-cube.xyz[index[0]][i])

def get_minimum_xyz_from_atom_name(cube, cml, atom_name, distance=2):
    atom = cml.dict_atoms[atom_name]
    tree = cKDTree(cube.xyz)
    k = round(len(cube.xyz)/len(cml.xyz))
    index = tree.query_ball_point([atom.xyz], distance)
    i = cube.values[index[0]].argmin()
    return cube.values[index[0]].min(), cube.xyz[index[0]][i]


def get_maximum_distance_from_atom_name(cube, cml, atom_name, distance = 2):
    atom = cml.dict_atoms[atom_name]
    tree = cKDTree(cube.xyz)
    k = round(len(cube.xyz)/len(cml.xyz))
    index = tree.query_ball_point([atom.xyz], distance)
    #print(distance)
    i = cube.values[index[0]].argmax()
    return cube.values[index[0]].max(), np.linalg.norm(atom.xyz-cube.xyz[index[0]][i])

def get_maximum_xyz_from_atom_name(cube, cml, atom_name, distance = 2):
    atom = cml.dict_atoms[atom_name]
    tree = cKDTree(cube.xyz)
    k = round(len(cube.xyz)/len(cml.xyz))
    index = tree.query_ball_point([atom.xyz], distance)
    #print(distance)
    i = cube.values[index[0]].argmax()
    return cube.values[index[0]].max(), cube.xyz[index[0]][i]

def get_maximum_distance_from_point(cube, point, distance = 2):
    tree = cKDTree(cube.xyz)
    dis, index = tree.query(point)
    #print(dis)
    return cube.values[index], dis
