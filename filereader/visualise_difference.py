import pandas as pd
import logging
import numpy as np
from scipy.spatial import cKDTree
import plotly
import plotly.graph_objs as go
import copy

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def color_size_tmesh(df, maximum = None, minimum=None, size=10):
    list_colors = []
    list_size = []
    if maximum==None:
        maximum = df["difference"].max()
    if minimum==None:
        minimum = df["difference"].min()
    
    if abs(maximum)>abs(minimum):
        threshold = maximum
    else:
        threshold = - minimum
    for i in df["difference"]:
        if i < 0:
            #if i > minimum:
            #    list_colours.append("white")
            #else:
            value = int(round(i*255/(-threshold)))
            list_colors.append('rgb({},{},{})'.format(255, 255-value, 255-value))
            list_size.append(size)
        else:
            if i > maximum:
                i=maximum
            value = int(round(i*255/threshold))
            list_colors.append('rgb({},{},{})'.format(255-value, 255-value, 255))
            list_size.append(size)
    LOGGER.info("MAXIMUM ABSOLUTE DIFFERENCE /kJmol-1: {}".format(2625.5*threshold))
    LOGGER.info("MAXIMUM MEP DFT/kJmol-1: {}".format(2625.5*df["dft"].max()))
    LOGGER.info("MINIMUM MEP DFT/kJmol-1: {}".format(2625.5*df["dft"].min()))
    return list_colors, list_size



def plot_instructions(df):
    x, y, z = df[0]*0.529177, df[1]*0.529177, df[2]*0.529177
    color, size= color_size_tmesh(df)
    # Configure Plotly to be rendered inline in the notebook.
    plotly.offline.init_notebook_mode()
    fig = go.Figure()


    # Configure the trace.
    trace = go.Scatter3d(
        x=x,  # <-- Put your data instead
        y=y,  # <-- Put your data instead
        z=z,  # <-- Put your data instead
        mode='markers',
        marker={
            'size': size,
            'opacity': 1.0,
            'color': color,
            'line':{'width':0}
        }
    )



    # Configure the layout.
    layout = go.Layout(
        margin={'l': 0, 'r': 0, 'b': 0, 't': 0}
    )

    data = [trace]

    plot_figure = go.Figure(data=data, layout=layout)
    return plot_figure, trace


def compare_cube_files(df, df_astex):

    df1 = copy.deepcopy(df_astex)
    array1 = np.array([df[0], df[1], df[2]]).T
    array2 = np.array([df1[0], df1[1], df1[2]]).T
    tree = cKDTree(array1)
    distance, index = tree.query(array2, k=1)
    
    list_dft = []
    list_difference = []
    list_dft_x = []
    list_dft_y = []
    list_dft_z = []
    for n, i in enumerate(index):
        #print(array1[i], array2[n])
        list_difference.append(-df.T[i][3]+df1.T[n][3])
        list_dft.append(df.T[i][3])
        list_dft_x.append(df.T[i][0])
        list_dft_y.append(df.T[i][1])
        list_dft_z.append(df.T[i][2])
    df1["dft_x"]=list_dft_x
    df1["dft_y"]=list_dft_y
    df1["dft_z"]=list_dft_z
    df1["dft"] = list_dft
    df1["difference"]=list_difference
    return df1


def read_cube_file(cube_file):
    df1 =pd.read_csv(cube_file, delimiter=" ", header = None, skiprows=2, nrows=1)
    df1.dropna(axis=1, how='all', inplace=True)
    df1=df1.T.reset_index().T
    skiplines = int(df1.loc[0][0]+6)

    df =pd.read_fwf(cube_file, header = None, skiprows=skiplines, widths=[13, 13, 13, 13])
    #df1 =pd.read_csv(cube_file, delimiter=" ", header = None, skiprows=2)

    return df
