import unittest
from filereader.ssip_reader import SsipReader
import os
FIXTURE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_files', '')

cml_water = f"{FIXTURE_DIR}/water_ssip.xml"

class TestSsipReader(unittest.TestCase):
    def setUp(self):
        self.ssip = SsipReader(cml_water)

    def test_list_atoms_length(self):
        self.assertEqual(len(self.ssip.list_atoms), 3)
   
    def test_atom_names(self):
        list_atoms = ['a1', "a2", "a3"]
        for i in self.ssip.list_atoms:
            self.assertTrue(i.aname in list_atoms)

    def test_list_bonds_length(self):
        self.assertEqual(len(self.ssip.list_bonds), 2)

    def test_list_ssips_length(self):
        self.assertEqual(len(self.ssip.list_ssips), 4)

    def test_ssip_values(self):
        dict_values = {
                i.atom_neigh.aname: i.value for i in self.ssip.list_ssips
                }
        self.assertAlmostEqual(dict_values["a2"], 2.9521548212110127)

if __name__ == '__main__':
    unittest.main()
