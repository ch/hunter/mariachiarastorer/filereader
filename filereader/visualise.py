import pandas as pd
import plotly
import plotly.graph_objs as go
from mendeleev import element
import re
from code_resources.pdb2cml import PdbToCml

def read_pqr(pqr_file):
    return  pd.read_fwf(pqr_file, header = None)

def color_size_pqr(df):
    list_colors = []
    list_size = []

    for j in df[2]:
        search = re.compile(r'[A-Za-z]+')
        mo=search.search(j)
        i = mo.group(0)
        if i[0]!= "H" or i=="HG" or i=="HO" or i=="HF" or i=="HS" or i=="HE":
            if len(i)==2:
                element_string = i[0].upper()+i[1].lower()
            elif len(i)==1:
                element_string=i.upper()
            color = str(element(element_string).cpk_color)
            list_colors.append(color)
            list_size.append(30)
        elif i== "Hh":
            list_colors.append("white")
            list_size.append(25)
        else:
            list_colors.append('black')
            list_size.append(10)
    return list_colors, list_size


def plot_instructions_pdb_pqr_new(pdb_file, pqr_file, skiprows=0):
    list_lines = get_list_lines(pdb_file, skiprows=skiprows)
    df2 = read_pqr(pqr_file)
    color, size = color_size_pqr(df2)

    x = list(df2[6])
    y = list(df2[7])
    z = list(df2[8])

    # Configure Plotly to be rendered inline in the notebook.
    plotly.offline.init_notebook_mode()
    fig = go.Figure()


    # Configure the trace.
    trace = go.Scatter3d(
        x=x,  # <-- Put your data instead
        y=y,  # <-- Put your data instead
        z=z,  # <-- Put your data instead
        mode='markers',
        marker={
            'size': size,
            'opacity': 1.0,
            'color': color,
            'line':{'width':0}
        }
    )



    # Configure the layout.
    layout = go.Layout(
        margin={'l': 0, 'r': 0, 'b': 0, 't': 0}
    )

    data = [trace]

    plot_figure = go.Figure(data=data, layout=layout)

    for i, j in list_lines:
        plot_figure.add_trace(go.Scatter3d(
            x=[i[0],j[0]], # <-- Put your data instead
            y=[i[1],j[1]],  # <-- Put your data instead
            z=[i[2],j[2]],  # <-- Put your data instead
            mode='lines',
            line={
                'color': 'black',
        }
        )
    )

    plot_figure.update_layout(showlegend=False)
    
    return plot_figure

def get_list_lines(pdb_file, skiprows = 0):
    pdb2cml =  PdbToCml(pdb_file, skiprows=skiprows)
    list_atoms = pdb2cml.list_atoms
    list_bonds = pdb2cml.list_bonds

    dict_xyz = {}
    for i in list_atoms:
        dict_xyz[i.aname]=i.xyz
    list_lines = []
    for i in list_bonds:
        list_lines.append((dict_xyz[i.atom1], dict_xyz[i.atom2]))
    return list_lines 
