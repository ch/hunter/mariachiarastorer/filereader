# Installing filereader #

Clone the repository

    git clone https://gitlab.developers.cam.ac.uk/ch/hunter/mariachiarastorer/filereader.git

Go into the new repository

    cd filereader

Create new environment (substitute "myenv" with your preferred name for the environment)

    conda create -n myenv --file spec-file.txt python=3.7

Activate enivironment

    conda activate myenv

Install filereader

    pip install .

Return to original directory

    cd ..






